/*
    This file is part of Triva.

    Triva is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Triva is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Triva.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __Triva_H_
#define __Triva_H_

#include "NSPointFunctions.h"
#include "TimeSliceGraph.h"
#include "TimeSliceTree.h"
#include "TimeSliceDifTree.h"
#include "Tree.h"
#include "TrivaFilter.h"
#include "TrivaGraphEdge.h"
#include "TrivaGraphNode.h"
#include "TrivaComposition.h"
#include "Triva.h"
#include "TrivaTreemap.h"
#include "TrivaWindow.h"

#endif
