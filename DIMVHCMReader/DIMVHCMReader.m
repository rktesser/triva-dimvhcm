/*
    This file is part of Triva.

    Triva is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Triva is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Triva.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "DIMVHCMReader.h"
#include <General/FoundationAdditions.h>
#include <General/UniqueString.h>
#include <General/Macros.h>

#define LINE_SIZE 512

extern NSLock *thaLock;

@implementation DIMVHCMReader

- (id)initWithController:(PajeTraceController *)c
{
    NSFileManager *fManager;
    self = [super initWithController:c];

    if (self != nil) {
        currentChunk = 0;
        hasMoreData = YES;
        userChunkSize = CHUNK_SIZE;

        chunkInfo = [[NSMutableArray alloc] initWithCapacity: 100];
        buffer = [[NSMutableArray alloc] initWithCapacity: 10240];
        bufferLock =  [[NSConditionLock alloc] initWithCondition: 0];
        headerCenter = [[PajeHeaderCenter alloc] initWithDefaultHeader];
        integrator = [[IntegratorLib alloc] init];
        clientId = nil;
        aggregatorNames = nil;
    	
        fManager = [[NSFileManager defaultManager] retain];
        NSString *fName = [[[NSHost currentHost] name] stringByAppendingString: @"-paje.output"];
        [fName retain];
        if([fManager createFileAtPath: fName contents: nil 
          attributes: nil] == NO){
           NSLog(@"ERROR: couldn't create the output file.");
    		return nil;
        }
        [fManager release];
        outFile = [NSFileHandle fileHandleForUpdatingAtPath: fName];
        [fName release];
    }
    return self;
}

- (void)dealloc
{
    if(aggregatorNames){
    	[aggregatorNames release];
    }
    if(clientId){
    	[clientId release];
    }
    [headerCenter release];
    [integrator release];
    [buffer release];
    [bufferLock release];
    [chunkInfo release];
    [outFile closeFile];
    [outFile release];
    [super dealloc];
    [super dealloc];
}

- (NSString *)traceDescription
{
	return @"DIMVisual";
}

// A new chunk will start.
// Position the file in the good position, if not yet there.
- (void)startChunk:(int)chunkNumber
{
    unsigned long position;
    if (chunkNumber != currentChunk) {
        if (chunkNumber >= [chunkInfo count]) {
            // cannot position in an unread place
            NSLog(@"Chunk after end: %d (%d)", chunkNumber, [chunkInfo count]);
            [self raise:@"Cannot start unknown chunk"];
        }
	position = [[chunkInfo objectAtIndex: chunkNumber] longLongValue];
	[outFile seekToFileOffset: position];
        hasMoreData = YES;
        currentChunk = chunkNumber;
    } else {
        // let's register the first chunk position
        if ([chunkInfo count] == 0) {
	    position = [outFile offsetInFile];
            [chunkInfo addObject: [NSNumber numberWithLongLong: position]];
        }
    }
    // keep the ball rolling (tell other components)
    [super startChunk: chunkNumber];
}

// The current chunk has ended.
- (void)endOfChunkLast:(BOOL)last
{
    unsigned long position;
//    NSLog(@"endOfChunkLast: %u", currentChunk -1);
    currentChunk++;
    if (!last){
	if(currentChunk == [chunkInfo count]){//The chunk is new.
            position = [outFile offsetInFile];
	    [chunkInfo addObject: [NSNumber numberWithLongLong: position]];
	}else{//The chunk has been reread.
	    currentChunk = ([chunkInfo count] - 1);
	    position = [[chunkInfo objectAtIndex: currentChunk] longLongValue];
            [outFile seekToFileOffset: position];
	}
    }
    [super endOfChunkLast: last];
}

- (void)raise:(NSString *)reason
{
    NSDebugLog(@"DIMVHCMReader: '%@'", reason);
    [[NSException exceptionWithName: @"PajeReadFileException" reason: reason
      userInfo: nil] raise];
}


- (void)inputEntity:(id)entity
{    
    [self raise:@"Configuration error:" " DIMVHCMReader should be first component"];
}

- (NSData *)getDataFromBuffer
{
	NSData *data;
	[bufferLock lockWhenCondition: 1];
        data = [[buffer objectAtIndex: 0] retain];
	[buffer removeObjectAtIndex: 0];
  	if([buffer count] == 0){
	    [bufferLock unlockWithCondition: 0];
	}else{
	    [bufferLock unlockWithCondition: 1];
	}
	return [data autorelease];
}

- (void)readNextChunk
{
    NSMutableData *chunk;
    NSData *data;
    unsigned long nextChunkPosition, chunkSize;
    BOOL canEndChunkBeforeData = NO;
    
    if (![self hasMoreData]) {
        return;
    }
    int nextChunk = currentChunk +1;
    if(nextChunk < [chunkInfo count]){
    	//Re-reading chunk. Just get it from the array.
    	//NSLog(@"Rereading chunk %u\n", currentChunk);
    	nextChunkPosition = [[chunkInfo objectAtIndex: nextChunk] 
	  longLongValue]; 
	chunkSize = nextChunkPosition - [outFile offsetInFile];
	chunk = (NSMutableData *)[outFile readDataOfLength: chunkSize];
	[chunk retain];
        [self outputEntity:[NSData dataWithData: chunk]];
    }else{
        // first time reading this chunk.
    	//NSLog(@"Reading chunk %u for the first time.", currentChunk);
    	chunk = [[NSMutableData alloc] init];
        data = [[self getDataFromBuffer] retain];
	do{
  	    [chunk appendData: data];
	    [data release];
            data = [[self getDataFromBuffer] retain];
	}while(([chunk length] + [data length]) < userChunkSize);
        [self outputEntity: [NSData dataWithData: chunk]];
	do{
	    canEndChunkBeforeData = [super canEndChunkBefore: 
	      [NSData dataWithData: data]];
	    if(canEndChunkBeforeData == NO){
		[chunk appendData: data];
		[data release];
            	data = [[self getDataFromBuffer] retain];
	    }
        }while(canEndChunkBeforeData == NO);
	[bufferLock lock];
	[buffer insertObject: data atIndex: 0];
	[bufferLock unlockWithCondition: 1];
	[outFile writeData: chunk];
//	[outFile synchronizeFile];
	[data release];
    }
    [chunk release];
}

- (BOOL)hasMoreData
{
    return hasMoreData;
}

- (void) setUserChunkSize: (unsigned long long)cs
{
    userChunkSize = cs;
}

- (BOOL)applyConfiguration: (NSDictionary *)conf
{
	NSArray *bundleNames;
	NSDictionary *bundleConfig;
	int i;
	
	[conf retain];
	
	clientId = [[conf objectForKey: @"id"] retain];
	aggregatorNames = [[conf objectForKey: @"aggregators"] retain];
	bundleNames = [conf objectForKey: @"bundles"];
	if(!(clientId && aggregatorNames && bundleNames)){
		NSLog(@"ERROR: The configuration file is incomplete.");
		return NO;
	}

	for (i = 0; i < [bundleNames count]; i++){
		[integrator loadDIMVisualBundle: [bundleNames objectAtIndex: i]];
		bundleConfig = [conf objectForKey: [[bundleNames
		 objectAtIndex: i] stringByAppendingString: @"_config"]];
		if(bundleConfig == nil){
			bundleConfig = [integrator 
			  getConfigurationOptionsFromDIMVisualBundle:
			  [bundleNames objectAtIndex: i]];
			
		}
		[integrator setConfiguration: bundleConfig forDIMVisualBundle:
		  @"dimvisual-ganglia-hcm.bundle"];
	}
	[conf release];
	return YES;
}


- (BOOL)putInTheBuffer: (NSData *)data
{
	[data retain];
	[bufferLock lock]; //it doesnt matter the condition, we just produce
	[buffer addObject: data];
	[bufferLock unlockWithCondition: 1];
	[data release];
	return YES;
}

//- (NSArray *)eventsAsData
- (NSData *)eventsAsData;
{
	NSMutableData *outData;
	NSMutableArray *events;
	LibPajeEvent *event;
	NSString *headerAsString;
	int i, code;

	outData = [[NSMutableData alloc] init];
	events = [integrator convert];
	[events retain];
	for(i = 0; i < [events	count]; i++){
		event = [events objectAtIndex: i];
		if([headerCenter headerIsPresent: [event header]] == NO){
			[headerCenter addHeader: [event header]];
			code = [headerCenter codeForHeader: [event header]];
			headerAsString = [headerCenter
			  printHeaderWithCode: code];
			[outData appendData: [headerAsString 
			  dataUsingEncoding: NSASCIIStringEncoding]];
		}
		[outData appendData: [[event printWithProvider: headerCenter]
		  dataUsingEncoding: NSASCIIStringEncoding]];
	}
	[events release];
	[outData autorelease];
	return outData;
}

- (void) launchDIMVClientWithArgs: (NSDictionary *)args
{
	[integrator launchDIMVClientWithId: [args objectForKey:@"CLIENTID"]
	  andAggregators: [args objectForKey:@"AGGREGATORS"]];
}

- (void) producer: (id) args
{	
	NSData *eventData;
	NSDictionary *clientArgs;

	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	NSLog(@"Will launch the DIMVClient now.");
	if(args == nil){
		clientArgs = [NSDictionary dictionaryWithObjectsAndKeys:
		  clientId, @"CLIENTID",
		  aggregatorNames, @"AGGREGATORS",
		  nil];
	}else{
		clientArgs = args;
	
	}
	[clientArgs retain];
	[self launchDIMVClientWithArgs: clientArgs];
	[clientArgs release];
	//Send the headers.
	NSLog(@"Sending the headers.");
	NSString *headerAsString = [headerCenter print];
	if(headerAsString != nil){
		[self putInTheBuffer: [headerAsString dataUsingEncoding:
                  NSASCIIStringEncoding]];
	}
	NSLog(@"Beginning the main producer loop of the DIMVHCMReader.");
	while ((eventData = [[self eventsAsData] retain]) != nil){
		[self putInTheBuffer: eventData];
		[eventData release];
		[thaLock lock];
		[pool emptyPool];
		[thaLock unlock];
	}
	[pool release];
}

- (NSString *)inputFilename
{
   return nil;
}

- (void)setInputFilename: (NSString *)filename
{
   return;
}

@end
