#import "HCMReader.h"

extern NSLock *theLock;

@implementation HCMReader

- (void) launchDIMVClientWithArgs: (NSDictionary *)args
{
	[integrator launchDIMVClientWithId: [args objectForKey:@"CLIENTID"]
	  andAggregators: [args objectForKey:@"AGGREGATORS"]];
}

- (id) initWithController: (id) cont
{
	NSFileManager *fManager;
	
	self = [super initWithController: cont];
	buffer = [[NSMutableArray alloc] init];
	bufferLock =  [[NSConditionLock alloc] initWithCondition: 0];
	headerCenter = [[PajeHeaderCenter alloc] initWithDefaultHeader];
	integrator = [[IntegratorLib alloc] init];
	clientId = nil;
	aggregatorNames = nil;
	
#ifdef HAVE_SUBSCRIPTIONMANAGER
//	subscrHandler = [[SubscriptionHandler alloc] init];
//	[integrator setHCMSubscriptionHandler: subscrHandler];
#endif

	fManager = [[NSFileManager defaultManager] retain];
	NSString *fName = [[[NSHost currentHost] name] stringByAppendingString: @"-paje.output"];
	[fName retain];
	if([fManager createFileAtPath: fName contents: nil 
	  attributes: nil] == NO){
		NSLog(@"ERROR: couldn't create the output file.");
		return nil;
	}
	[fManager release];
	outFile = [NSFileHandle fileHandleForWritingAtPath: fName];
	[fName release];
	
	return self;
}

- (BOOL)applyConfiguration: (NSDictionary *)conf
{
	NSArray *bundleNames;
	NSDictionary *bundleConfig;
	int i;
	
	[conf retain];
	
	clientId = [[conf objectForKey: @"id"] retain];
	aggregatorNames = [[conf objectForKey: @"aggregators"] retain];
	bundleNames = [conf objectForKey: @"bundles"];
	if(!(clientId && aggregatorNames && bundleNames)){
		NSLog(@"ERROR: The configuration file is incomplete.");
		return NO;
	}

	for (i = 0; i < [bundleNames count]; i++){
		[integrator loadDIMVisualBundle: [bundleNames objectAtIndex: i]];
		bundleConfig = [conf objectForKey: [[bundleNames
		 objectAtIndex: i] stringByAppendingString: @"_config"]];
		if(bundleConfig == nil){
			bundleConfig = [integrator 
			  getConfigurationOptionsFromDIMVisualBundle:
			  [bundleNames objectAtIndex: i]];
			
		}
		[integrator setConfiguration: bundleConfig forDIMVisualBundle:
		  @"dimvisual-ganglia-hcm.bundle"];
	}
	[conf release];
	return YES;
}

- (void) dealloc
{
	if(aggregatorNames){
		[aggregatorNames release];
	}
	if(clientId){
		[clientId release];
	}
	[headerCenter release];
	[integrator release];
	[buffer release];
	[bufferLock release];
	[super dealloc];
}

- (NSString *)traceDescription
{
	return @"HCMReader";
}

//This method runs the main consumer loop.
- (void) waitForDataFromHCM: (id) object
{
	NSData *data;
	int i;
	int long long chunkNumber = 0;
	BOOL canEndChunkBeforeData = YES;
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
        /*canEndChunkBeforeData needs to true in the first iteration in
	 * order to start the first chunk.*/
	while (1){ //wait for data forever
		//Obtain lock when buffer has data.
		[bufferLock lockWhenCondition: 1];
		for(i = 0; i < [buffer count]; i++){
			data = [buffer objectAtIndex: i];
			[data retain];
			/*The first time the outer while loop is executed,
			 * we will start a chunk because (canEndChunk 
			 * beforeData == YES) is true.*/
			if(canEndChunkBeforeData == YES){
//			NSLog(@"New chunk: %d", chunkNumber);
//				[outFile writeData: data];
//				[outFile synchronizeFile];
				[theLock lock];
				[super startChunk: chunkNumber++];
				[self outputEntity: data];
				[theLock unlock];
				canEndChunkBeforeData = NO;
			}else{
				[theLock lock];
				canEndChunkBeforeData = [self canEndChunkBefore: [[NSData dataWithData: data] retain]];
				[theLock unlock];
			}
			if(canEndChunkBeforeData){
			/*Note: The call to [self canEndChunkBeforeData: data]
			 * modifies the data passed as argument. So we needed 
			 * to copy it because we may need to reuse it in case
			 * the test returns YES.*/
//				NSLog(@"End Chunk");
				[theLock lock];
				[self endOfChunkLast: 0];
				[theLock unlock];
//				NSLog(@"Chunk has ended!");
				[theLock lock];
				[controller timeLimitsChanged];
				[controller setSelectionStartTime: [controller 
				  startTime] endTime: [controller endTime]];
				[theLock unlock];
				/* The method canEndChunk beforeData sends
				 * the data to the outputEntity when the result
				 * is false. Otherwise, we need to start a 
				 * new chunk with the data passed as argument. 
				 * So we decrease the loop counter in order to 
				 * "re-read" the data.*/
				[data release];
				i --;
			}else{
//				NSLog(@"Same chunk: %d", chunkNumber);
//				[outFile writeData: data];
//				[outFile synchronizeFile];
			}
		}
		[buffer removeAllObjects];
		[bufferLock unlockWithCondition: 0]; 
	}
	[pool release];
	return;
}

- (BOOL) sendToPaje: (NSData *) data
{
	[data retain];
	[bufferLock lock]; //it doesnt matter the condition, we just produce
	[buffer addObject: data];
	[bufferLock unlockWithCondition: 1];
	[data release];
	return YES;
}

- (NSArray *)eventsAsData
{
	NSMutableArray *outData;
	NSMutableArray *events;
	LibPajeEvent *event;
	NSString *headerAsString;
	NSString *headerLine;
	NSArray *headerAsStringArray;
	int i, j, code;

	outData = [[NSMutableArray alloc] init];
	events = [integrator convert];
	[events retain];
	for(i = 0; i < [events	count]; i++){
		event = [events objectAtIndex: i];
		if([headerCenter headerIsPresent: [event header]] == NO){
			[headerCenter addHeader: [event header]];
			code = [headerCenter codeForHeader: [event header]];
			headerAsString = [headerCenter
			  printHeaderWithCode: code];
			headerAsStringArray = [headerAsString 
			  componentsSeparatedByString: @"\n"];
			for(j = 0; j < [headerAsStringArray count]; j++){
				headerLine = [headerAsStringArray 
				  objectAtIndex: j];
 				if([headerLine length] != 0){
					[outData addObject: [[headerLine 
					  stringByAppendingString: @"\n"] 
					  dataUsingEncoding:
					    NSASCIIStringEncoding]];
				}
			}
		}
		[outData addObject: [[event printWithProvider: headerCenter]
		  dataUsingEncoding: NSASCIIStringEncoding]];
	}
	[outData autorelease];
	return outData;
}

- (void) producer: (id) args
{	
	NSArray *dataArray;
	NSArray *headerAsStringArray;
	NSString *headerLine;
	NSDictionary *clientArgs;
	int i;

	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	

	NSLog(@"Will launch the DIMVClient now.");
	if(args == nil){
		clientArgs = [NSDictionary dictionaryWithObjectsAndKeys:
		  clientId, @"CLIENTID",
		  aggregatorNames, @"AGGREGATORS",
		  nil];
	}else{
		clientArgs = args;
	
	}
	[clientArgs retain];
	[self launchDIMVClientWithArgs: clientArgs];
	[clientArgs autorelease];
	//Send the headers.
	NSLog(@"Sending the headers.");
//	NSLog(@"%@", [headerCenter print]);
/*	[self sendToPaje: [[headerCenter print] dataUsingEncoding: 
	  NSASCIIStringEncoding]];
*/
	headerAsStringArray = [[headerCenter print] componentsSeparatedByString: @"\n"];
	for(i = 0; i < [headerAsStringArray count]; i++){
//		NSLog(@"header line %d: %@", i, [headerAsStringArray objectAtIndex: i]);
		if([[headerAsStringArray objectAtIndex: i] length] != 0){
			headerLine = [[headerAsStringArray objectAtIndex: i] 
			 stringByAppendingString: @"\n"];
			[self sendToPaje: [headerLine dataUsingEncoding:
			  NSASCIIStringEncoding]];

		}
	}
	NSLog(@"Beginning the main producer loop of the HCMReader.");
	while ((dataArray = [[self eventsAsData] retain]) != nil){
//		[NSThread sleepForTimeInterval: 1.1]; //sleep for 1.1 secs
//		NSLog (@"%s", __FUNCTION__);

                for(i = 0; i < [dataArray count] ; i++){
			[self sendToPaje: [dataArray objectAtIndex: i]];
		}
		[dataArray release];
		[pool emptyPool];
	}
	[pool release];
}

@end
